#! /usr/bin/bash

rm -rf bin
cd src
echo "compiling..."
flex++ bore.lex
g++ lex.yy.cc
cp a.out bore
mkdir ../bin
mv bore ../bin
echo "compiled"
echo "cleaning up..."
rm lex.yy.cc
rm a.out
echo "cleaned up"
echo "done"
