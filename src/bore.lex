%{
    #include <iostream>
    #include <regex>
    #define DEBUG
    using namespace std;
%}

%option noyywrap c++

str     \".*\"
char    \'.\'
ws      [ \t]+
dig     [0-9]
num     [-+]?{dig}+
float   [-+]?{dig}+\.{dig}+([eE][-+]?{dig}+)?

%%

{ws}
{num}     cout << "number: " << YYText() << '\n';
{float}   cout << "float: " << YYText() << '\n';
{str}     cout << "string: " << YYText() << '\n';
{char}    cout << "charecter: " << YYText() << '\n';
"+"       cout << "plus\n";
"-"       cout << "minus\n";
"*"       cout << "mutlipy\n";
"/"       cout << "divided by\n";
"%"       cout << "modulus\n";
"^"       cout << "to the power of\n";
":"       cout << "collon\n";
";"       cout << "semi-collon\n";
"\\"      cout << "escape\n";
#ifdef DEBUG
"exit"    exit(0);
#endif

%%

int main(int argc, char** argv) {
    cout << "args: " << argv << '\n';
    FlexLexer* lexer = new yyFlexLexer;
    while(lexer->yylex() != 0) return 0;
}